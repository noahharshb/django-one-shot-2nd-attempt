from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import PostForm, TodoListForm

# Create your views here.
def show_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    tasks = TodoItem.objects.filter(list=todo, id=id)
    context = {
        "todo_object": todo,
        'tasks': tasks,
    } 
    return render(request, "todos/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = PostForm()

    context = {
        "post_form": form,
    }
    return render(request, "todos/create.html", context)