from django.urls import path
from todos.views import show_list, todo_list_detail, create_todo_list

urlpatterns = [
    path("", show_list, name="todo_list_list" ),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    #path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    #path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    #path("items/create/", create_todo_item, name="todo_item_create"),
    #path("items/edit/", update_todo_item, name="todo_item_edit"), 
]