# Generated by Django 5.0 on 2023-12-14 18:01

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todolist",
            name="created_on",
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
